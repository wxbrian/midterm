//
//  StudentResultViewController.swift
//  C0727922_MidTerm_MADF2018
//
//  Created by Admin on 2018-02-28.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class StudentResultViewController: UIViewController {
    
    @IBOutlet weak var lblData: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblData.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblData.numberOfLines = 10
        
        let utdList = Student.getAllStudents()
        var s = String()
        for (key, value) in utdList{
            s +=  "\nName: \(value.studentName!)\n Mark: \(value.studentMark!)\n Email: \(value.studentEmail!)\n Course: \(value.studentCourse!)\n Gender: \(value.studentGender!)\n BirthDate: \(value.studentBD!)\n ID: \(value.studentID!)"
        }
        print(s)
        lblData.text = s
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
