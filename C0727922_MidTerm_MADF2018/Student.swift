//
//  Student.swift
//  C0727922_MidTerm_MADF2018
//
//  Created by Admin on 2018-02-28.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class Student{
    var studentID: String!
    var studentName: String!
    var studentGender: String!
    var studentCourse: String!
    var studentEmail: String!
    var studentBD: String!
    var studentMark: String!
    
    private static var studentList = [String:Student]()
    
    init(){
        self.studentID = "C0727922"
        self.studentName = "BRIAN SILVA"
        self.studentBD = "18/02/1994"
        self.studentGender = "HIM"
        self.studentCourse = "MADT"
        self.studentEmail = "BRIANJXD@GMAIL.COM"
        self.studentMark = "A+"
    }
    
    init(_ studentID:String, _ studentName: String, _ studentBD: String, _ studentGender: String, studentCourse: String, studentEmail: String, studentMark: String){
        
        self.studentID = studentID
        self.studentName = studentName
        self.studentBD = studentBD
        self.studentGender = studentGender
        self.studentCourse = studentCourse
        self.studentEmail = studentEmail
        self.studentMark = studentMark
    }
    
    static func addStudent(std: Student) -> Bool{
        
        if self.studentList[std.studentID] == nil{
            self.studentList[std.studentID] = std
            return true
        }
        return false
    }
    
    static func getStudentByID(studentID: String) -> Student{
        
        if self.studentList[studentID] != nil{
            return self.studentList[studentID]!
        }
        return Student()
    }
    
    static func getAllStudents() -> [String:Student] {
        return studentList
    }

    
    
}
