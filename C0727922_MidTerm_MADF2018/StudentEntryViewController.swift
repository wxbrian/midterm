//
//  StudentEntryViewController.swift
//  C0727922_MidTerm_MADF2018
//
//  Created by Admin on 2018-02-28.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class StudentEntryViewController: UIViewController {

    @IBOutlet weak var studentID: UITextField!
    @IBOutlet weak var studentName: UITextField!
    @IBOutlet weak var studentGender: UITextField!
    @IBOutlet weak var studentCourse: UITextField!
    @IBOutlet weak var studentEmail: UITextField!
    @IBOutlet weak var studentBD: UITextField!
    @IBOutlet weak var studentMark: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func saveButton(_ sender: UIButton) {
        
        var student = Student()
        student.studentID = studentID.text!
        student.studentName = studentName.text!
        student.studentGender = studentGender.text!
        student.studentCourse = studentCourse.text!
        student.studentEmail = studentEmail.text!
        student.studentBD = studentBD.text!
        student.studentMark = studentMark.text!
        
        let length = studentName.text
        
        if ((length?.count)! > 10) {
            let alert  =
                UIAlertController(title: "Error", message: "Name tooo big", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        let flag = Student.addStudent(std: student)
        
        if flag == true{
            print("Student Record Saved")
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController = storyBoard.instantiateViewController(withIdentifier: "ResultVC") as! StudentResultViewController
            self.present(loginViewController, animated: true, completion: nil)
        }else{
            let alert  =
                UIAlertController(title: "Error", message: "Possible duplicated error", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
