//
//  ViewController.swift
//  C0727922_MidTerm_MADF2018
//
//  Created by Admin on 2018-02-28.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var myRememberMe: UISwitch!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    var myUserDefault: UserDefaults?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        if let Email = myUserDefault?.value(forKey: "userEmail"){
            userEmail.text = userEmail as? String
        }
        
        if let Password = myUserDefault?.value(forKey: "userPassword"){
            userPassword.text = userPassword as? String
        }
    }

    @IBAction func loginTap(_ sender: Any) {
        if(userEmail.text == "fo" && userPassword.text == "ba")
        {
            if (self.myRememberMe.isOn && userEmail.text != ""){
                self.myUserDefault?.set(userEmail.text, forKey: "userEmail")
                self.myUserDefault?.set(userPassword.text, forKey: "userPassword")
            }else{
                self.myUserDefault?.removeObject(forKey: "userName")
                self.myUserDefault?.removeObject(forKey: "userPassword")
            }
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let loginViewController = storyBoard.instantiateViewController(withIdentifier: "StudentVC") as! StudentEntryViewController
            self.present(loginViewController, animated: true, completion: nil)
        }
        else{
            let alert  =
                UIAlertController(title: "Error", message: "User Email / Password incorrect", preferredStyle: UIAlertControllerStyle.alert)
            
            let actionOk = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(actionOk)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

